![jgpu_bb.png](https://bitbucket.org/repo/jqGreR/images/1998400113-jgpu_bb.png)

Repositorio con los materiales utilizados en las jornadas de divulgación de aplicaciones científicas y visión 
por computador sobre procesadores gráficos.

Puedes encontrar las presentaciones y los laboratorios que desarrollamos en el aula de supercomputación L14 de la EPS.

Cualquier duda o sugerencia podeis mandárnosla al siguiente buzón:

**jgpu@dtic.ua.es**